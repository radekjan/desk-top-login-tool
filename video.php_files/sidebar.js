// JavaScript Document

function isCollapsed(elem) {
	var element = document.getElementById(elem);
	var state = (element.style.display == 'block') ? false : true;
	return state;
}

function toggleGroup(elem) {
	var element = document.getElementById(elem);
	var collapsed = isCollapsed(elem);
	collapseAll();
	if(!collapsed) {
		hideGroup(elem);
	} else {
		showGroup(elem);
	}
}

function showGroup(elem) {
	var element = document.getElementById(elem);
	element.style.display = 'block';
	element.style.position = 'static';
}

function hideGroup(elem) {
	var element = document.getElementById(elem);
	element.style.display = 'none';
	element.style.position = 'absolute';
}

function collapseAll() {
	for(var i=1; i<=15; i++) {
		hideGroup("lesson"+i);	
	}	
}