﻿
using System.Windows.Forms;
using System;
using CookComputing.XmlRpc;

[XmlRpcUrl("http://drummondgeometry.com/xml-rpc/account_server.php")]
public interface IAccount : IXmlRpcProxy
{
    
    [XmlRpcMethod("account.login")]
    string Login(string email, string pw);
}