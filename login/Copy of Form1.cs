﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace login
{
    public partial class Form1 : Form
    {

        private bool minimized = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.email != null)
                txtEmail.Text = Properties.Settings.Default.email;
            if (Properties.Settings.Default.pw != null)
                txtPw.Text = Properties.Settings.Default.pw;
            if (Properties.Settings.Default.remember != false)
                chxRemember.CheckState = CheckState.Checked;
            this.ActiveControl = txtEmail;
        }
        private void Form1_Closing(object sender, EventArgs e)
        {
            if (chxRemember.Checked)
            {
                Properties.Settings.Default.email = txtEmail.Text.Trim();
                Properties.Settings.Default.pw = txtPw.Text.Trim();
                Properties.Settings.Default.remember = true;
            }
            else
            {
                Properties.Settings.Default.email = null;
                Properties.Settings.Default.pw = null;
                Properties.Settings.Default.remember = false;
            }
            Properties.Settings.Default.Save();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            pnlLoading.Visible = true;
            pnlLoginForm.Enabled = false;
            
            string email = txtEmail.Text.Trim();
            string pw = txtPw.Text.Trim();

            if (!email.Contains("@"))
            {
                txtEmail.BackColor = System.Drawing.Color.Lavender;
                lblEmail.ForeColor = System.Drawing.Color.Red;
                lblEmail.Font = lblErrorFont.Font;
            }
            else
            {
                txtEmail.BackColor = System.Drawing.SystemColors.Window;
                lblEmail.ForeColor = System.Drawing.Color.Black;
                lblEmail.Font = lblGoodFont.Font;
            }
            MakeCall(email, pw);
            pnlLoading.Visible = false;
            pnlLoginForm.Enabled = true;
        }

        private void OpenLink(string link)
        {
            System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo();

            Info.FileName = link;

            System.Diagnostics.Process Proc;

            try
            {
                Proc = System.Diagnostics.Process.Start(Info);
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                lblMsg.Text = ex.Message;
                return;
            }
        }

        private void MakeCall(string email, string pw)
        {
            WebRequest req = null;
            WebResponse rsp = null;
            try
            {
                string uri = "http://stage.drummondgeometry.com/xml-rpc/account_server.php";

                req = WebRequest.Create(uri);
                //req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy

                req.Method = "POST";        // Post method

                req.ContentType = "text/xml";     // content type

                req.Timeout = 5000;     // timeout 5 seconds

                // Wrap the request stream with a text-based streamwriter
                StreamWriter writer = new StreamWriter(req.GetRequestStream());

                // Write the xml text into the stream
                writer.WriteLine("\n\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<methodResponse>\n"+
                    "<params>\n" +
                    "<param>\n" +
                    "<value><string>" + email +
                    "</string></value>\n" +
                    "</param>\n" +
                    "<param>\n" +
                    "<value><string>" + pw +
                    "</string></value>\n" +
                    "</param>\n" +
                    "</params>\n"+
                    "</methodResponse>\n");
                writer.Close();
                
                // Send the data to the webserver
                rsp = req.GetResponse();
            }
            catch(WebException webEx)
            {

            }
            catch(Exception ex)
            {

            }
            finally
            {
                try{
                    if(req != null) req.GetRequestStream().Close();
                    if(rsp != null) rsp.GetResponseStream().Close();
                }
                catch(Exception ex)
                {
                    lblStack.Text = "error " + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace + " ";

                }
                lblMsg.Text += req.ToString();
            }
        }

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void menuItemShow_Click(object sender, EventArgs e)
        {
            if (minimized)
            {
                minimized = false;
                menuItemShow.Text = "Hide";
                Show();
            }
            else
            {
                minimized = true;
                menuItemShow.Text = "Show";
                Hide();
            }
        }

        private void sysTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            menuItemShow_Click(this, null);
        }
    }
}
