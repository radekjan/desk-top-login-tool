﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using CookComputing.XmlRpc;

namespace login
{
    public partial class Form1 : Form
    {

        private bool minimized = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.email != null)
                txtEmail.Text = Properties.Settings.Default.email;
            if (Properties.Settings.Default.pw != null)
                txtPw.Text = Properties.Settings.Default.pw;
            if (Properties.Settings.Default.remember != false)
                chxRemember.CheckState = CheckState.Checked;
            this.ActiveControl = txtEmail;
        }

        private void Form1_HelpButtonClicked(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            OpenLink("http://www.drummondgeometry.com/support.php");
        }

        private void Form1_FormClosing(object sender, EventArgs e)
        {
            if (chxRemember.Checked)
            {
                Properties.Settings.Default.email = txtEmail.Text.Trim();
                Properties.Settings.Default.pw = txtPw.Text.Trim();
                Properties.Settings.Default.remember = true;
            }
            else
            {
                Properties.Settings.Default.email = null;
                Properties.Settings.Default.pw = null;
                Properties.Settings.Default.remember = false;
            }
            Properties.Settings.Default.Save();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {            
            string email = txtEmail.Text.Trim();
            string pw = txtPw.Text.Trim();
            bool error = false;

            if (pw.Length < 6)
            {
                txtPw.BackColor = System.Drawing.Color.Lavender;
                lblPw.ForeColor = System.Drawing.Color.Red;
                lblPw.Font = lblErrorFont.Font;
                this.ActiveControl = txtPw;
                txtPw.Focus();
                error = true;
            }
            else
            {
                txtPw.BackColor = System.Drawing.SystemColors.Window;
                lblPw.ForeColor = System.Drawing.Color.Black;
                lblPw.Font = lblGoodFont.Font;
            }

            if (!email.Contains("@"))
            {
                txtEmail.BackColor = System.Drawing.Color.Lavender;
                lblEmail.ForeColor = System.Drawing.Color.Red;
                lblEmail.Font = lblErrorFont.Font;
                txtEmail.Focus();
                error = true;
            }
            else
            {
                txtEmail.BackColor = System.Drawing.SystemColors.Window;
                lblEmail.ForeColor = System.Drawing.Color.Black;
                lblEmail.Font = lblGoodFont.Font;
            }
            if (!error)
            {
                Thread call = new Thread(new ThreadStart(MakeCall));
                call.IsBackground = true;

                Cursor = Cursors.WaitCursor;
                pnlLoginForm.Enabled = false;
                pnlLoading.Visible = true;
                call.Start();
                //while (call.IsAlive) { lblMsg.Text = call.ThreadState.ToString(); }

            }
            form_state_login_form();
        }

        private void OpenLink(string link)
        {
            System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo();

            Info.FileName = link;
            System.Diagnostics.Process Proc;

            try
            {
                Proc = System.Diagnostics.Process.Start(Info);
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                lblMsg.Text = ex.Message;
                return;
            }
        }

        public void MakeCall()
        {
            IAccount account = (IAccount)XmlRpcProxyGen.Create(typeof(IAccount));
            account.Credentials = new NetworkCredential("xmlrpcseclogin", "xml2531fairfax");

            string retstr = "";

            try
            {

                string email = txtEmail.Text.Trim();
                string pw = txtPw.Text.Trim();

                retstr = account.Login(email, pw);
                lblStack.Text = retstr;
                if (retstr == "error")
                    lblMsg.Text = "Your email and password were not found.";
                else
                {
                    lblMsg.Text = "Please enter your email and password to login.";
                    menuItemShow_Click(this, null);
                    sysTray.ShowBalloonTip(3000);

                    menuItemReconnect.Visible = true;

                    pnlLoginForm.Enabled = true;
                    pnlLoginForm.Visible = false;
                    pnlReLogin.Visible = true;

                    OpenLink("https://www.drummondgeometry.com/sec_login.php?cid=" + retstr);
                    menuItemShow_Click(this, null);
                }
                
            }
            catch (Exception ex)
            {
                form_state_login_form();
                HandleException(ex);
            }

            Cursor = Cursors.Default;
        }

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void sysTray_BalloonTipClicked(object sender, EventArgs e)
        {
        }

        private void menuItemShow_Click(object sender, EventArgs e)
        {
            if (minimized)
            {
                minimized = false;
                menuItemShow.Text = "Hide";
                Show();
            }
            else
            {
                minimized = true;
                menuItemShow.Text = "Show";
                Hide();
            }
        }

        private void sysTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            menuItemShow_Click(this, null);
        }


        private void HandleException(Exception ex)
        {
            string msgBoxTitle = "Error";
            try
            {
                throw ex;
            }
            catch (XmlRpcFaultException fex)
            {
                form_state_login_form();
                MessageBox.Show("Fault Response: " + fex.FaultCode + " "
                  + fex.FaultString, msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (WebException webEx)
            {
                form_state_login_form();
                MessageBox.Show("WebException: " + webEx.Message, msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (webEx.Response != null)
                    webEx.Response.Close();
            }
            catch (Exception excep)
            {
                form_state_login_form();
                MessageBox.Show(excep.Message, msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReLogin_Click(object sender, EventArgs e)
        {
            btnSubmit_Click(this, null);
        }

        public void form_state_login_form()
        {
            pnlLoginForm.Enabled = true;
            pnlLoginForm.Visible = true;
            pnlReLogin.Visible = false;
        }

        public void form_state_relogin()
        {
            pnlLoginForm.Enabled = true;
            pnlLoginForm.Visible = false;
            pnlReLogin.Visible = true;
        }


        public void form_state_loading()
        {
            pnlLoginForm.Enabled = false;
            pnlLoginForm.Visible = true;
            pnlReLogin.Visible = false;
        }

        private void menuItemReconnect_Click(object sender, EventArgs e)
        {
            menuItemShow_Click(this, null);
            btnSubmit_Click(this, null);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
