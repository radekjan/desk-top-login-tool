﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using CookComputing.XmlRpc;

namespace login
{
    public partial class Form1 : Form
    {

        private bool minimized = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Microsoft.Win32.RegistryKey OurKey =
                Microsoft.Win32.Registry.CurrentUser.CreateSubKey("Software\\dg_login");

            string email = "";
            string pw = "";
            string remember = "0";

            if (OurKey != null)
            {
                email = (string)OurKey.GetValue("email");
                pw = (string)OurKey.GetValue("pw");
                remember = (string)OurKey.GetValue("remember");
            }

            foreach (string Keyname in OurKey.GetSubKeyNames())
            {
                MessageBox.Show(Keyname);
            }

            if (email != null)
                txtEmail.Text = email;
            if (pw != null)
                txtPw.Text = DecodeFrom64(pw);
            if (remember != null && remember == "1")
                chxRemember.CheckState = CheckState.Checked;
            else
                chxRemember.CheckState = CheckState.Unchecked;
            this.ActiveControl = txtEmail;
        }

        static public string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }

        static public string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);

            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;

        }

        // Need next line in the designer. It goes away sometimes
        // this.HelpButtonClicked +=new System.ComponentModel.CancelEventHandler(Form1_HelpButtonClicked);
        private void Form1_HelpButtonClicked(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            frmAbout frm = new frmAbout();
            frm.ShowDialog(this);
        }

        // Need next line in the designer. It goes away sometimes
        // this.FormClosing +=new System.Windows.Forms.FormClosingEventHandler(Form1_FormClosing);
        private void Form1_FormClosing(object sender, EventArgs e)
        {
            Microsoft.Win32.RegistryKey OurKey =
                Microsoft.Win32.Registry.CurrentUser.CreateSubKey("Software\\dg_login");
            
            if (chxRemember.Checked)
            {
                OurKey.SetValue("email", txtEmail.Text.Trim());
                OurKey.SetValue("pw", EncodeTo64(txtPw.Text.Trim()));
                OurKey.SetValue("remember", "1");
            }
            else if(OurKey != null)
            {
                OurKey.SetValue("email", "");
                OurKey.SetValue("pw", "");
                OurKey.SetValue("remember", "0");
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {            
            string email = txtEmail.Text.Trim();
            string pw = txtPw.Text.Trim();
            bool error = false;

            if (pw.Length < 6)
            {
                txtPw.BackColor = System.Drawing.Color.Lavender;
                lblPw.ForeColor = System.Drawing.Color.Red;
                lblPw.Font = lblErrorFont.Font;
                this.ActiveControl = txtPw;
                txtPw.Focus();
                error = true;
            }
            else
            {
                txtPw.BackColor = System.Drawing.SystemColors.Window;
                lblPw.ForeColor = System.Drawing.Color.Black;
                lblPw.Font = lblGoodFont.Font;
            }

            if (!email.Contains("@"))
            {
                txtEmail.BackColor = System.Drawing.Color.Lavender;
                lblEmail.ForeColor = System.Drawing.Color.Red;
                lblEmail.Font = lblErrorFont.Font;
                txtEmail.Focus();
                error = true;
            }
            else
            {
                txtEmail.BackColor = System.Drawing.SystemColors.Window;
                lblEmail.ForeColor = System.Drawing.Color.Black;
                lblEmail.Font = lblGoodFont.Font;
            }
            if (!error)
            {
                form_state_loading();
                MakeCall(email, pw);
            }
            form_state_login_form();
        }

        private void OpenLink(string link)
        {
            System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo();

            Info.FileName = link;
            System.Diagnostics.Process Proc;

            try
            {
                Proc = System.Diagnostics.Process.Start(Info);
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                lblMsg.Text = ex.Message;
                return;
            }
        }

        private void MakeCall(string email, string pw)
        {
            IAccount account = (IAccount)XmlRpcProxyGen.Create(typeof(IAccount));
            account.Credentials = new NetworkCredential("xmlrpcseclogin", "xml2531fairfax");

            string retstr = "";
            Cursor = Cursors.WaitCursor;

            lblLoading.Visible = true;

            Application.DoEvents();
            try
            {
            retstr = account.Login(email, pw);

                lblStack.Text = retstr;
                if (retstr == "error")
                    lblMsg.Text = "Your email and password were not found.";
                else
                {
                    lblMsg.Text = "Please enter your email and password to login.";
                    menuItemShow_Click(this, null);
                    sysTray.ShowBalloonTip(3000);

                    menuItemReconnect.Visible = true;

                    pnlLoginForm.Enabled = true;
                    pnlLoginForm.Visible = false;
                    pnlReLogin.Visible = true;

                    OpenLink("https://www.drummondgeometry.com/sec_login.php?cid=" + retstr);
                    //menuItemShow_Click(this, null);
                }
                
            }
            catch (Exception ex)
            {

                lblStack.Text = retstr;
                form_state_login_form();
                HandleException(ex);
            }

            lblLoading.Visible = false;
            Cursor = Cursors.Default;
        }

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void sysTray_BalloonTipClicked(object sender, EventArgs e)
        {
        }

        private void menuItemShow_Click(object sender, EventArgs e)
        {
            if (minimized)
            {
                minimized = false;
                menuItemShow.Text = "Hide";
                Show();
            }
            else
            {
                minimized = true;
                menuItemShow.Text = "Show";
                Hide();
            }
        }

        private void sysTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            menuItemShow_Click(this, null);
        }


        private void HandleException(Exception ex)
        {
            string msgBoxTitle = "Error";
            try
            {
                throw ex;
            }
            catch (XmlRpcFaultException fex)
            {
                form_state_login_form();
                MessageBox.Show("Fault Response: " + fex.FaultCode + " "
                  + fex.FaultString, msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (WebException webEx)
            {
                form_state_login_form();
                MessageBox.Show("WebException: " + webEx.Message, msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (webEx.Response != null)
                    webEx.Response.Close();
            }
            catch (Exception excep)
            {
                form_state_login_form();
                MessageBox.Show(excep.ToString(), msgBoxTitle,
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReLogin_Click(object sender, EventArgs e)
        {
            btnSubmit_Click(this, null);
        }

        public void form_state_login_form()
        {
            lblLoading.Visible = false;
            pnlLoginForm.Enabled = true;
            pnlLoginForm.Visible = true;
            pnlReLogin.Visible = false;
        }

        public void form_state_relogin()
        {
            lblLoading.Visible = false;
            pnlLoginForm.Enabled = true;
            pnlLoginForm.Visible = false;
            pnlReLogin.Visible = true;
        }


        public void form_state_loading()
        {
            lblLoading.Visible = true;
            pnlLoginForm.Enabled = false;
            pnlLoginForm.Visible = true;
            pnlReLogin.Visible = false;
        }

        private void menuItemReconnect_Click(object sender, EventArgs e)
        {
            menuItemShow_Click(this, null);
            btnSubmit_Click(this, null);
        }

        private void lblMsg_Click(object sender, EventArgs e)
        {

        }

        private void menuItemAbout_Click(object sender, EventArgs e)
        {

            frmAbout frm = new frmAbout();
            frm.ShowDialog(this);
        }
    }
}
