﻿namespace login
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.picBanner = new System.Windows.Forms.PictureBox();
            this.pnlLoginForm = new System.Windows.Forms.Panel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.lblPw = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtPw = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.chxRemember = new System.Windows.Forms.CheckBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblLoading = new System.Windows.Forms.Label();
            this.lblGoodFont = new System.Windows.Forms.Label();
            this.lblErrorFont = new System.Windows.Forms.Label();
            this.pnlReLogin = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReLogin = new System.Windows.Forms.Button();
            this.sysTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.sysTrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuItemReconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemShow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.lblStack = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).BeginInit();
            this.pnlLoginForm.SuspendLayout();
            this.pnlReLogin.SuspendLayout();
            this.sysTrayMenu.SuspendLayout();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(Form1_FormClosing);
            this.SuspendLayout();
            // 
            // picBanner
            // 
            this.picBanner.Image = ((System.Drawing.Image)(resources.GetObject("picBanner.Image")));
            this.picBanner.Location = new System.Drawing.Point(0, 0);
            this.picBanner.Name = "picBanner";
            this.picBanner.Size = new System.Drawing.Size(400, 80);
            this.picBanner.TabIndex = 0;
            this.picBanner.TabStop = false;
            // 
            // pnlLoginForm
            // 
            this.pnlLoginForm.Controls.Add(this.lblMsg);
            this.pnlLoginForm.Controls.Add(this.lblPw);
            this.pnlLoginForm.Controls.Add(this.lblEmail);
            this.pnlLoginForm.Controls.Add(this.txtPw);
            this.pnlLoginForm.Controls.Add(this.txtEmail);
            this.pnlLoginForm.Controls.Add(this.chxRemember);
            this.pnlLoginForm.Controls.Add(this.btnSubmit);
            this.pnlLoginForm.Location = new System.Drawing.Point(12, 147);
            this.pnlLoginForm.Name = "pnlLoginForm";
            this.pnlLoginForm.Size = new System.Drawing.Size(368, 134);
            this.pnlLoginForm.TabIndex = 1;
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.Location = new System.Drawing.Point(-1, 3);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(207, 17);
            this.lblMsg.TabIndex = 6;
            this.lblMsg.Text = "Enter your email and password:";
            this.lblMsg.Click += new System.EventHandler(this.lblMsg_Click);
            // 
            // lblPw
            // 
            this.lblPw.AutoSize = true;
            this.lblPw.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPw.Location = new System.Drawing.Point(198, 24);
            this.lblPw.Name = "lblPw";
            this.lblPw.Size = new System.Drawing.Size(69, 17);
            this.lblPw.TabIndex = 5;
            this.lblPw.Text = "Password";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(-1, 25);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(103, 17);
            this.lblEmail.TabIndex = 4;
            this.lblEmail.Text = "E-mail Address";
            // 
            // txtPw
            // 
            this.txtPw.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPw.Location = new System.Drawing.Point(201, 44);
            this.txtPw.Name = "txtPw";
            this.txtPw.PasswordChar = '*';
            this.txtPw.Size = new System.Drawing.Size(150, 20);
            this.txtPw.TabIndex = 2;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(2, 44);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(150, 20);
            this.txtEmail.TabIndex = 1;
            // 
            // chxRemember
            // 
            this.chxRemember.AutoSize = true;
            this.chxRemember.Location = new System.Drawing.Point(203, 82);
            this.chxRemember.Name = "chxRemember";
            this.chxRemember.Size = new System.Drawing.Size(121, 17);
            this.chxRemember.TabIndex = 3;
            this.chxRemember.Text = "Save my information";
            this.chxRemember.UseVisualStyleBackColor = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(201, 105);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(77, 23);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Login";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblLoading
            // 
            this.lblLoading.AutoSize = true;
            this.lblLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoading.ForeColor = System.Drawing.Color.Maroon;
            this.lblLoading.Location = new System.Drawing.Point(6, 228);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(182, 17);
            this.lblLoading.TabIndex = 9;
            this.lblLoading.Text = "Verifying login credentials...";
            this.lblLoading.Visible = false;
            // 
            // lblGoodFont
            // 
            this.lblGoodFont.AutoSize = true;
            this.lblGoodFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoodFont.Location = new System.Drawing.Point(243, 301);
            this.lblGoodFont.Name = "lblGoodFont";
            this.lblGoodFont.Size = new System.Drawing.Size(75, 17);
            this.lblGoodFont.TabIndex = 8;
            this.lblGoodFont.Text = "Good Font";
            this.lblGoodFont.Visible = false;
            // 
            // lblErrorFont
            // 
            this.lblErrorFont.AutoSize = true;
            this.lblErrorFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorFont.Location = new System.Drawing.Point(317, 301);
            this.lblErrorFont.Name = "lblErrorFont";
            this.lblErrorFont.Size = new System.Drawing.Size(82, 17);
            this.lblErrorFont.TabIndex = 7;
            this.lblErrorFont.Text = "Error Font";
            this.lblErrorFont.Visible = false;
            // 
            // pnlReLogin
            // 
            this.pnlReLogin.Controls.Add(this.label1);
            this.pnlReLogin.Controls.Add(this.btnReLogin);
            this.pnlReLogin.Location = new System.Drawing.Point(178, 115);
            this.pnlReLogin.Name = "pnlReLogin";
            this.pnlReLogin.Size = new System.Drawing.Size(158, 74);
            this.pnlReLogin.TabIndex = 7;
            this.pnlReLogin.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 26);
            this.label1.TabIndex = 9;
            this.label1.Text = "Click login to,\r\nre-establish your connection.";
            // 
            // btnReLogin
            // 
            this.btnReLogin.Location = new System.Drawing.Point(32, 48);
            this.btnReLogin.Name = "btnReLogin";
            this.btnReLogin.Size = new System.Drawing.Size(75, 23);
            this.btnReLogin.TabIndex = 9;
            this.btnReLogin.Text = "Login";
            this.btnReLogin.UseVisualStyleBackColor = true;
            this.btnReLogin.Click += new System.EventHandler(this.btnReLogin_Click);
            // 
            // sysTray
            // 
            this.sysTray.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.sysTray.BalloonTipText = "Click on icon to reconnect";
            this.sysTray.BalloonTipTitle = "DG Secure Login";
            this.sysTray.ContextMenuStrip = this.sysTrayMenu;
            this.sysTray.Icon = ((System.Drawing.Icon)(resources.GetObject("sysTray.Icon")));
            this.sysTray.Text = "Drummond Geometry\r\nLearning Center\r\nSecure Login";
            this.sysTray.Visible = true;
            this.sysTray.BalloonTipClicked += new System.EventHandler(this.sysTray_BalloonTipClicked);
            this.sysTray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.sysTray_MouseDoubleClick);
            // 
            // sysTrayMenu
            // 
            this.sysTrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemReconnect,
            this.menuItemShow,
            this.menuItemAbout,
            this.menuItemExit});
            this.sysTrayMenu.Name = "contextMenuStrip1";
            this.sysTrayMenu.Size = new System.Drawing.Size(154, 92);
            // 
            // menuItemReconnect
            // 
            this.menuItemReconnect.Name = "menuItemReconnect";
            this.menuItemReconnect.Size = new System.Drawing.Size(153, 22);
            this.menuItemReconnect.Text = "Re-connect";
            this.menuItemReconnect.Visible = false;
            this.menuItemReconnect.Click += new System.EventHandler(this.menuItemReconnect_Click);
            // 
            // menuItemShow
            // 
            this.menuItemShow.Name = "menuItemShow";
            this.menuItemShow.Size = new System.Drawing.Size(153, 22);
            this.menuItemShow.Text = "Hide";
            this.menuItemShow.Click += new System.EventHandler(this.menuItemShow_Click);
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Name = "menuItemAbout";
            this.menuItemAbout.Size = new System.Drawing.Size(153, 22);
            this.menuItemAbout.Text = "About...";
            this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
            // 
            // menuItemExit
            // 
            this.menuItemExit.Name = "menuItemExit";
            this.menuItemExit.Size = new System.Drawing.Size(153, 22);
            this.menuItemExit.Text = "E&xit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // lblStack
            // 
            this.lblStack.AutoSize = true;
            this.lblStack.Location = new System.Drawing.Point(450, 42);
            this.lblStack.Name = "lblStack";
            this.lblStack.Size = new System.Drawing.Size(0, 13);
            this.lblStack.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Please sign in to view secure member content.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(278, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Welcome to Drummond Geometry";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(10, 140);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(371, 2);
            this.panel1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 295);
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblGoodFont);
            this.Controls.Add(this.lblStack);
            this.Controls.Add(this.lblErrorFont);
            this.Controls.Add(this.pnlLoginForm);
            this.Controls.Add(this.picBanner);
            this.Controls.Add(this.pnlReLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(Form1_HelpButtonClicked);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "DG Secure Login";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBanner)).EndInit();
            this.pnlLoginForm.ResumeLayout(false);
            this.pnlLoginForm.PerformLayout();
            this.pnlReLogin.ResumeLayout(false);
            this.pnlReLogin.PerformLayout();
            this.sysTrayMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.PictureBox picBanner;
        private System.Windows.Forms.Panel pnlLoginForm;
        private System.Windows.Forms.NotifyIcon sysTray;
        private System.Windows.Forms.Label lblPw;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtPw;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.CheckBox chxRemember;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label lblErrorFont;
        private System.Windows.Forms.Label lblGoodFont;
        private System.Windows.Forms.ContextMenuStrip sysTrayMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItemShow;
        private System.Windows.Forms.ToolStripMenuItem menuItemExit;
        private System.Windows.Forms.Label lblStack;
        private System.Windows.Forms.Button btnReLogin;
        private System.Windows.Forms.Panel pnlReLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem menuItemReconnect;
        private System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem menuItemAbout;
    }
}

