﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace login
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!IsAppAlreadyRunning())
                Application.Run(new Form1());
            else
            {
                MessageBox.Show("You are already running the DG Secure Login Application\nIt is located in your system tray.",
                    "DG Secure Login",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }
        public static bool IsAppAlreadyRunning()
        {
            Process currentProcess = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                if (currentProcess.Id != process.Id)
                {
                    if (currentProcess.ProcessName == process.ProcessName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
